## Test application for TTP

### To build the app 

```shell
./mvnw clean install
```

### Start servers for the application

```shell
cd etc/docker/local/
docker-compose -f docker-compose-servers.yml up -d
```
