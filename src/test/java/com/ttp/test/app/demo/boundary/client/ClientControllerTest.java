package com.ttp.test.app.demo.boundary.client;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.ttp.test.app.demo.boundary.client.model.RegisterClientRequest;
import com.ttp.test.app.demo.boundary.client.model.RegisterClientResponse;
import com.ttp.test.app.demo.service.ClientService;
import io.zonky.test.db.AutoConfigureEmbeddedDatabase;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.MvcResult;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.springframework.test.web.servlet.result.MockMvcResultMatchers;

import java.util.UUID;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.mockito.Mockito.when;

@AutoConfigureMockMvc
@SpringBootTest
@AutoConfigureEmbeddedDatabase
class ClientControllerTest {
    @Autowired
    private MockMvc mockMvc;
    @Autowired
    private ObjectMapper objectMapper;
    @MockBean
    private ClientService clientService;

    @Test
    void testRegisterClient() throws Exception {
        // given
        String clientId = UUID.randomUUID().toString();

        RegisterClientRequest request = new RegisterClientRequest();
        request.setClientId(clientId);
        request.setReferalCode("1");
        request.setLandingPageUrl("https://landing.com/page");
        request.setUserAgent("Mozilla firefox x.x");
        request.setCallerIp("10.10.0.2");

        // when
        String clickId = UUID.randomUUID().toString();
        when(clientService.registerClient(request.getClientId(), request.getReferalCode(), request.getLandingPageUrl(),
                request.getUserAgent(), request.getCallerIp())).thenReturn(clickId);

        MvcResult result = mockMvc.perform(
                        MockMvcRequestBuilders
                                .post("/register/client")
                                .contentType(MediaType.APPLICATION_JSON)
                                .content(objectMapper.writeValueAsString(request)))
                .andExpect(MockMvcResultMatchers.status().isOk())
                .andReturn();


        // then
        String resultJson = result.getResponse().getContentAsString();
        assertNotNull(resultJson);

        RegisterClientResponse response = objectMapper.readValue(result.getResponse().getContentAsString(), RegisterClientResponse.class);
        assertEquals(clickId, response.getId());
    }
}