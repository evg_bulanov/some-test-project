package com.ttp.test.app.demo.dao;

import com.ttp.test.app.demo.dao.model.AffiliateClient;
import io.zonky.test.db.AutoConfigureEmbeddedDatabase;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.transaction.annotation.Transactional;

import java.util.Optional;
import java.util.UUID;

import static org.junit.jupiter.api.Assertions.assertTrue;

@DataJpaTest
@AutoConfigureEmbeddedDatabase
class AffiliateClientRepositoryTest {
    @Autowired
    AffiliateClientRepository repository;

    @Test
    @Transactional
    void findById() {
        // given:
        String uuid = UUID.randomUUID().toString();
        AffiliateClient client = new AffiliateClient().setClientId(uuid);

        repository.save(client);

        // when:
        Optional<AffiliateClient> actual = repository.findById(uuid);

        // then:
        assertTrue(actual.isPresent());
    }
}