package com.ttp.test.app.demo.dao;

import com.ttp.test.app.demo.dao.model.FailedCall;
import io.zonky.test.db.AutoConfigureEmbeddedDatabase;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.transaction.annotation.Transactional;

import java.util.Optional;
import java.util.UUID;

import static org.junit.jupiter.api.Assertions.assertTrue;

@DataJpaTest
@AutoConfigureEmbeddedDatabase
class FailedCallRepositoryTest {
    @Autowired
    FailedCallRepository repository;

    @Test
    @Transactional
    void findById() {
        // given:
        String clientUuid = UUID.randomUUID().toString();
        String failureUuid = UUID.randomUUID().toString();
        FailedCall failedCall = new FailedCall()
                .setFailureId(failureUuid)
                .setClientId(clientUuid)
                .setRequestType("createClick");


        repository.save(failedCall);

        // when:
        Optional<FailedCall> actual = repository.findById(failureUuid);

        // then:
        assertTrue(actual.isPresent());
    }
}