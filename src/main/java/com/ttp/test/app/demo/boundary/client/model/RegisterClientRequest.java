package com.ttp.test.app.demo.boundary.client.model;

import lombok.Getter;
import lombok.Setter;

@Getter @Setter
public class RegisterClientRequest {
    /**
     * clientId that will contain a UUID
     */
    private String clientId;
    /**
     * referalCode that will contain an alphanumeric value
     */
    private String referalCode;
    /**
     * landing page that will contain a URL
     */
    private String landingPageUrl;
    /**
     * userAgent that will contain the browser type of the caller
     */
    private String userAgent;
    /**
     * ip that will contain the IP of the caller
     */
    private String callerIp;
}