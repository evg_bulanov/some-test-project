package com.ttp.test.app.demo.dao;

import com.ttp.test.app.demo.dao.model.FailedCall;
import org.springframework.data.jpa.repository.JpaRepository;

public interface FailedCallRepository extends JpaRepository<FailedCall, String> {
}
