package com.ttp.test.app.demo.dao.model;

import jakarta.persistence.Column;
import jakarta.persistence.Entity;
import jakarta.persistence.Id;
import jakarta.persistence.Table;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.Setter;
import lombok.experimental.Accessors;

import java.sql.Timestamp;

@Entity
@Table(name = "failed_calls")
@Getter @Setter @Accessors(chain = true) @EqualsAndHashCode
public class FailedCall {
    /**
     * failure UUID
     */
    @Id
    @Column(name = "failure_id")
    private String failureId;

    /**
     * client UUID
     */
    @Column(name = "client_id")
    private String clientId;

    /**
     * request type, ex: “createClick”
     * todo create enum
     */
    @Column(name = "request_type")
    private String requestType;

    /**
     * Request payload
     */
    @Column(name = "payload")
    private String payload;

    /**
     * reason of failure, exception message
     */
    @Column(name = "reason_of_failure")
    private String reasonOfFailure;


    @Column(name = "time_of_failure")
    private Timestamp timeOfFailure;

    /**
     * Request processed status, false in case of failure
     */
    @Column(name = "processed")
    private Boolean processed;
}
