package com.ttp.test.app.demo.dao;

import com.ttp.test.app.demo.dao.model.AffiliateClient;
import org.springframework.data.jpa.repository.JpaRepository;

public interface AffiliateClientRepository extends JpaRepository<AffiliateClient, String> {
}
