package com.ttp.test.app.demo.boundary.client;

import com.ttp.test.app.demo.boundary.client.model.RegisterClientRequest;
import com.ttp.test.app.demo.boundary.client.model.RegisterClientResponse;
import com.ttp.test.app.demo.service.ClientService;
import lombok.RequiredArgsConstructor;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequiredArgsConstructor
public class ClientController {
    private final ClientService clientService;

    @PostMapping(value = "/register/client", consumes = "application/json")
    public RegisterClientResponse registerClient(@RequestBody RegisterClientRequest request) {
        String clickId = clientService.registerClient(request.getClientId(), request.getReferalCode(),
                request.getLandingPageUrl(), request.getUserAgent(), request.getCallerIp());
        return new RegisterClientResponse(clickId);
    }
}
