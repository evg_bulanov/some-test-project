package com.ttp.test.app.demo.dao.model;

import jakarta.persistence.Column;
import jakarta.persistence.Entity;
import jakarta.persistence.Id;
import jakarta.persistence.Table;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.Setter;
import lombok.experimental.Accessors;

import java.sql.Timestamp;

@Entity
@Table(name = "affiliate_client_map")
@Getter @Setter @Accessors(chain = true) @EqualsAndHashCode
public class AffiliateClient {
    /**
     * client UUID
     */
    @Id
    @Column(name = "client_id")
    private String clientId;

    /**
     * alphanumeric value
     */
    @Column(name = "referral_code")
    private String referralCode;

    /**
     * click UUID
     */
    @Column(name = "click_id")
    private String clickId;

    /**
     * user agent, browser type of the caller
     */
    @Column(name = "user_agent")
    private String userAgent;

    /**
     * Create date, default current time stamp in db
     */
    @Column(name = "creation_date")
    private Timestamp creationDate;
}


