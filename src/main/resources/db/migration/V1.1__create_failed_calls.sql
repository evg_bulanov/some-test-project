CREATE TABLE failed_calls (
    failure_id VARCHAR(255) NOT NULL,
    client_id VARCHAR(255) NOT NULL,
    request_type VARCHAR(255) NOT NULL,
    payload TEXT,
    reason_of_failure TEXT,
    time_of_failure timestamp DEFAULT CURRENT_TIMESTAMP,
    processed BOOLEAN,
    PRIMARY KEY (failure_id)
);

CREATE UNIQUE INDEX failed_calls_failure_id_idx ON failed_calls (failure_id);
CREATE UNIQUE INDEX failed_calls_client_id_idx ON failed_calls (client_id);
CREATE UNIQUE INDEX failed_calls_time_of_failure_idx ON failed_calls (time_of_failure);
