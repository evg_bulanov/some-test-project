CREATE TABLE affiliate_client_map (
    client_id VARCHAR(255) NOT NULL,
    referral_code VARCHAR(255) NOT NULL,
    click_id VARCHAR(255) NOT NULL,
    user_agent VARCHAR(255) NOT NULL,
    creation_date timestamp DEFAULT CURRENT_TIMESTAMP,
    PRIMARY KEY (client_id)
);

CREATE UNIQUE INDEX affiliate_client_map_client_id_idx ON affiliate_client_map (client_id);
CREATE UNIQUE INDEX affiliate_client_map_referral_code_idx ON affiliate_client_map (referral_code);
CREATE UNIQUE INDEX affiliate_client_map_click_id_idx ON affiliate_client_map (click_id);
